﻿
using Models;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class TileMesh : MonoBehaviour
{
    public Texture2D[] textures;

    const int tileSize = 32;
    const float colorScale = 100;

    Mesh mesh;
    MeshCollider meshCollider;
    MeshRenderer meshRenderer;

    List<Vector3> vertices;
    List<int> triangles;

    Dictionary<string, int> textureMap;
    Texture2DArray textureArray;
    List<Color> color;
    List<Vector3> uv;
    List<Vector3> left;


    World world;

   
    void Awake()
    {
        // Get the components from gameobjects
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        meshCollider = gameObject.AddComponent<MeshCollider>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();

        mesh.name = "Tile Mesh";
        vertices = new List<Vector3>();
        triangles = new List<int>();
        color = new List<Color>();
        uv = new List<Vector3>();
        left = new List<Vector3>();

        world = new World(10, 10);

        // Generate texture array for tile shader.
        GenerateTextureArray();

        Triangulate(world.AllTiles());
    }

    public void GenerateTextureArray()
    {
        var size = textures.Length;

        // Instantiate array and lookup table.
        textureMap = new Dictionary<string, int>();
        textureArray = new Texture2DArray(tileSize, tileSize, size, TextureFormat.RGB24, false);

        // Iterrate through all available tile types used in the game.
        for (int i=0; i < size; i++)
        {
            // Get texture for the tiletype.
            var texture = textures[i];

            // Check if texture has the correct size.
            if(texture.width != tileSize || texture.height != tileSize)
            { 
                Debug.LogErrorFormat("The texture {0} does not have the required size of {1}x{1}.", textures[i].name, tileSize);
            }

            // Copy texture into the texture array.
            Graphics.CopyTexture(texture, 0, 0, textureArray, i, 0);
            textureMap[textures[i].name] = i;
        }

        // Apply the texture to the mesh.
        meshRenderer.material.mainTexture = textureArray;
    }

    public void Triangulate(Tile[] tiles)
    {
        // Cleare old data
        mesh.Clear();
        vertices.Clear();
        triangles.Clear();
        color.Clear();
        uv.Clear();
        left.Clear();

        // Calculate new data
        for (int i = 0; i < tiles.Length; i++)
        {
            Triangulate(tiles[i]);
        }

        // Apply data to mesh.
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.colors = color.ToArray();

        mesh.SetUVs(0, uv);
        mesh.SetUVs(1, left);

        mesh.RecalculateNormals();
        meshCollider.sharedMesh = mesh;
    }

    void Triangulate(Tile tile)
    {
        // Gets the center point and calculaate triangles.
        var tileCenter = new Vector3(tile.Position.X, tile.Position.Y);
        var tileTopLeft = tileCenter + new Vector3(-0.5f, 0.5f);
        var tileTopRigth = tileCenter + new Vector3(0.5f, 0.5f);
        var tileBottomLeft = tileCenter + new Vector3(-0.5f, -0.5f);
        var tileBottomRight = tileCenter + new Vector3(0.5f, -0.5f);

        AddTriangle(tileBottomLeft, tileTopLeft, tileTopRigth);
        AddTriangle(tileBottomLeft, tileTopRigth, tileBottomRight);


        // Get  terrain typ and calculate uvs for it.
        var terrainID = textureMap[tile.TileType];
        var terrainTopLeft = new Vector3(0, 1, terrainID);
        var terrainTopRight = new Vector3(1, 1, terrainID);
        var terrainBottomLeft = new Vector3(0, 0, terrainID);
        var terrainBottomRight = new Vector3(1, 0, terrainID);

        AddUV(terrainBottomLeft, terrainTopLeft, terrainTopRight);
        AddUV(terrainBottomLeft, terrainTopRight, terrainBottomRight);

        var topLeft = world.GetTileAt(tile.Position.Offset(-1, 1));
        if (topLeft == null)
            topLeft = tile;

        var top = world.GetTileAt(tile.Position.Offset(0, 1));
        if (top == null)
            top = tile;

        var topRight = world.GetTileAt(tile.Position.Offset(1, 1));
        if (topRight == null)
            topRight = tile;

        var right = world.GetTileAt(tile.Position.Offset(1, 0));
        if (right == null)
            right = tile;
       
        var bottomRight = world.GetTileAt(tile.Position.Offset(1, -1));
        if (bottomRight == null)
            bottomRight = tile;

        var bottom = world.GetTileAt(tile.Position.Offset(0, -1));
        if (bottom == null)
            bottom = tile;

        var bottomLeft = world.GetTileAt(tile.Position.Offset(-1, -1));
        if (bottomLeft == null)
            bottomLeft = tile;

        var left = world.GetTileAt(tile.Position.Offset(-1, 0));
        if (left == null)
            left = tile;

        var colorBL = new Color(textureMap[left.TileType] / colorScale, textureMap[tile.TileType] / colorScale, textureMap[bottom.TileType] / colorScale, textureMap[bottomLeft.TileType] / colorScale);
        var colorTL = new Color(textureMap[topLeft.TileType] / colorScale, textureMap[top.TileType] / colorScale, textureMap[tile.TileType] / colorScale, textureMap[left.TileType] / colorScale);
        var colorTR = new Color(textureMap[top.TileType] / colorScale, textureMap[topRight.TileType] / colorScale, textureMap[right.TileType]/ colorScale, textureMap[tile.TileType] / colorScale);
        var colorBR = new Color(textureMap[tile.TileType] / colorScale, textureMap[right.TileType] / colorScale, textureMap[bottomRight.TileType] / colorScale, textureMap[bottom.TileType] / colorScale);

        AddColor(colorBL, colorTL, colorTR);
        AddColor(colorBL, colorTR, colorBR);
        


        // Get  terrain typ of the left tile and calculate uvs for it.
        var leftTile = tile;
        if(tile.Position.X > 0)
            leftTile = world.GetTileAt(tile.Position.Offset(-1, 0));

        terrainID = textureMap[leftTile.TileType];
        terrainTopLeft = new Vector3(0, 1, terrainID);
        terrainTopRight = new Vector3(1, 1, terrainID);
        terrainBottomLeft = new Vector3(0, 0, terrainID);
        terrainBottomRight = new Vector3(1, 0, terrainID);

        AddLeftUV(terrainBottomLeft, terrainTopLeft, terrainTopRight);
        AddLeftUV(terrainBottomLeft, terrainTopRight, terrainBottomRight);
        
    }

    void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        int vertexIndex = vertices.Count;
        vertices.Add(v1);
        vertices.Add(v2);
        vertices.Add(v3);
        triangles.Add(vertexIndex);
        triangles.Add(vertexIndex + 1);
        triangles.Add(vertexIndex + 2);
    }

    void AddUV(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        uv.Add(v1);
        uv.Add(v2);
        uv.Add(v3);
    }

    void AddLeftUV(Vector3 v1, Vector3 v2, Vector3 v3)
    {
        left.Add(v1);
        left.Add(v2);
        left.Add(v3);
    }

    void AddColor(Color color1, Color color2, Color color3)
    {
        color.Add(color1);
        color.Add(color2);
        color.Add(color3);
    }
}
