﻿using System;
using System.Linq;


namespace Models
{
    public struct TilePosition
    {
        public TilePosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }

        public int Y { get; }


        public static TilePosition operator +(TilePosition left, TilePosition right)
        {
            return new TilePosition(left.X + right.X, left.Y + right.Y);
        }

        public static TilePosition operator -(TilePosition left, TilePosition right)
        {
            return new TilePosition(left.X - right.X, left.Y - right.Y);
        }

        public static TilePosition operator *(TilePosition left, int right)
        {
            return new TilePosition(left.X * right, left.Y * right);
        }

        public static TilePosition operator *(int left, TilePosition right)
        {
            return new TilePosition(left * right.X, left * right.Y);
        }

        public static TilePosition operator /(TilePosition left, int right)
        {
            return new TilePosition(left.X / right, left.Y / right);
        }


        public static bool operator ==(TilePosition left, TilePosition right)
        {
            return left.X == right.X && left.Y == right.Y;
        }

        public static bool operator !=(TilePosition left, TilePosition right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Get a tilepositon with the offset from the current one.
        /// </summary>
        /// <param name="x"> Offset in x direction. </param>
        /// <param name="y"> Offset in y direction. </param>
        /// <returns>Returns a new tilepostion with the given offset. </returns>
        public TilePosition Offset(int x, int y)
        {
            return new TilePosition(X + x, Y + y);
        }

        /// <summary>
        /// Calculates the distance between two points.
        /// </summary>
        /// <param name="pos1">First point.</param>
        /// <param name="pos2">Secound point.</param>
        /// <returns>Returns the distance between the two points.</returns>
        public static float Distance(TilePosition pos1, TilePosition pos2)
        {
            return (float)Math.Sqrt(Math.Pow(pos1.X - pos2.X, 2) + Math.Pow(pos1.Y - pos2.Y, 2));
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            TilePosition p = (TilePosition)obj;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y);
        }

        public bool Equals(TilePosition p)
        {
            // If parameter is null return false:
            if ((object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == p.X) && (Y == p.Y);
        }

        public override int GetHashCode()
        {
            return X ^ Y;
        }


        public override string ToString()
        {
            return X + ";" + Y;
        }


        public static TilePosition Parse(string text)
        {
            char[] delimiterChars = { ';' };
            var numbers = text.Split(delimiterChars);

            if (numbers.Count() != 2)
            {
                throw new FormatException("String does not have the right format. The allowed format is 'X;Y' where X and Y is a number");
            }

            var x = int.Parse(numbers[0]);
            var y = int.Parse(numbers[1]);

            return new TilePosition(x, y);
        }
    }
}