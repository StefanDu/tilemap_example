﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models
{
    public class World
    {
        public int Width { get; set; }
        public int Height { get; set; }

        private Tile[,] _tiles;

        public World(int width, int height)
        {
            Width = width;
            Height = height;

            GenerateEmptyWorld();
        }

        private void GenerateEmptyWorld()
        {
            // Generate empty tiles on the world
            _tiles = new Tile[Width, Height];
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    var tileType = "Blue";
                    if (y < 5)
                    {
                        if (x % 3 == 0)
                            tileType = "Green";
                        if (x % 3 == 1)
                            tileType = "Red";
                    }
                    else
                    {
                        if (y % 3 == 0)
                            tileType = "Green";
                        if (y % 3 == 1)
                            tileType = "Red";
                    }

                    _tiles[x, y] = new Tile
                    {
                        TileType = tileType,
                        Position = new TilePosition(x, y)
                    };
                }
            }
        }

        public Tile GetTileAt(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                return null;

            return _tiles[x, y];
        }

        public Tile GetTileAt(TilePosition position)
        {
            return GetTileAt(position.X, position.Y);
        }

        public Tile[] AllTiles()
        {
            // Get array of all tiles.
            Tile[] tiles = new Tile[Width * Height];
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    // Get tile model data
                    var tileData = GetTileAt(x, y);
                    tiles[x * Height + y] = tileData;
                }
            }

            return tiles;
        }
    }
}
