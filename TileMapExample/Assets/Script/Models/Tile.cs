﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    public class Tile
    {
        public string TileType { get; set; }

        public TilePosition Position { get; set; }
    }
}