﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TileMap" {
	Properties{
		_MainTex("Texture", 2DArray) = "white" {}
		_BlendTex("Blend", 2D) = "White" {}
	}
		SubShader{
		Pass{
			CGPROGRAM
				#pragma vertex MyVertexProgram
				#pragma fragment MyFragmentProgram
				#pragma target 3.5
				// #include "UnityCG.cginc"

				UNITY_DECLARE_TEX2DARRAY(_MainTex);
				sampler2D _BlendTex;

				struct VertexData {
					float4 position : POSITION;
					float3 uv : TEXCOORD0;
					float3 left : TEXCOORD1;
				};

				struct Interpolators {
					float4 position : SV_POSITION;
					float3 uv : TEXCOORD0;
					float3 left : TEXCOORD1;
				};

				Interpolators MyVertexProgram(VertexData v) {
					Interpolators i;
					i.position = UnityObjectToClipPos(v.position);
					i.uv = v.uv;
					i.left = v.left;
					return i;
				}

				fixed4 MyFragmentProgram(Interpolators i) : SV_TARGET {
					fixed4 main = UNITY_SAMPLE_TEX2DARRAY(_MainTex, i.uv);
					fixed4 left = UNITY_SAMPLE_TEX2DARRAY(_MainTex, i.left);

					fixed4 blend = tex2D(_BlendTex, i.uv);

					return main.rgba * blend.a + (1 - blend.a) * left.rgba;
				}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
